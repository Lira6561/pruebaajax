<?php
use Migrations\AbstractMigration;

class User extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
            $tabla = $this->table('users');
        $tabla
        ->addColumn('nombre', 'string')
        ->addColumn('apellido_p', 'string')
        ->addColumn('apellido_m', 'string')
        ->addColumn('correo', 'string')
        ->addColumn('tipo_u', 'boolean')
        ->addColumn('pwd','string')
        ->addColumn('carrera','string')
        ->addColumn('facultad','string')
        ->addColumn('created', 'timestamp', ['default' => 'CURRENT_TIMESTAMP','timezone'=>'America/Mexico City'])
        ->create();
    }
}
