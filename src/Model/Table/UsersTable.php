<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 255)
            ->requirePresence('nombre', 'create')
            ->allowEmptyString('nombre', false);

        $validator
            ->scalar('apellido_p')
            ->maxLength('apellido_p', 255)
            ->requirePresence('apellido_p', 'create')
            ->allowEmptyString('apellido_p', false);

        $validator
            ->scalar('apellido_m')
            ->maxLength('apellido_m', 255)
            ->requirePresence('apellido_m', 'create')
            ->allowEmptyString('apellido_m', false);

        $validator
            ->scalar('correo')
            ->maxLength('correo', 255)
            ->requirePresence('correo', 'create')
            ->allowEmptyString('correo', false);

        $validator
            ->boolean('tipo_u')
            ->requirePresence('tipo_u', 'create')
            ->allowEmptyString('tipo_u', false);

        $validator
            ->scalar('pwd')
            ->maxLength('pwd', 255)
            ->requirePresence('pwd', 'create')
            ->allowEmptyString('pwd', false);

        $validator
            ->scalar('carrera')
            ->maxLength('carrera', 255)
            ->requirePresence('carrera', 'create')
            ->allowEmptyString('carrera', false);

        $validator
            ->scalar('facultad')
            ->maxLength('facultad', 255)
            ->requirePresence('facultad', 'create')
            ->allowEmptyString('facultad', false);

        return $validator;
    }
}
