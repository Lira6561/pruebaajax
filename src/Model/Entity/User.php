<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellido_p
 * @property string $apellido_m
 * @property string $correo
 * @property bool $tipo_u
 * @property string $pwd
 * @property string $carrera
 * @property string $facultad
 * @property \Cake\I18n\FrozenTime $created
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nombre' => true,
        'apellido_p' => true,
        'apellido_m' => true,
        'correo' => true,
        'tipo_u' => true,
        'pwd' => true,
        'carrera' => true,
        'facultad' => true,
        'created' => true
    ];
}
