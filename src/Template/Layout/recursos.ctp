<!DOCTYPE html>
<html>

<head>
  
       <?= $this->Html->script('Prueba.js') ?>
    <!-- Include external files and scripts here (See HTML helper for more info.) -->
    <?php
        echo $this->Html->css('recursos.css');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>
</head>

<body>
    <?php echo $this->element('Header/cabecera'); ?>

    <?=$this->Flash->render() ?>
    <div class="container clearfix">
        <!-- Aquí es donde las vistas se muestran. -->
        <?php echo $this->fetch('content'); ?>
    </div>
    <footer>
    </footer>
</body>

</html>