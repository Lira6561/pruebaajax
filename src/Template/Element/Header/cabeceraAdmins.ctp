
<header class="header" id="header" role="banner">
    
    <div class="row">
          <div class="col">
            <?php
                echo $this->Html->link(
                    $this->Html->image(
                        'https://www.unam.mx/sites/all/themes/unam/logo.png',
                        ['alt' => 'Inicio']
                    ),
                    ['controller' => 'medios', 'action' => 'index'],
                    ['class' => 'header__logo-image', 'escape' => false]
                );
                 ?>
              </div>
      
     </div>
    <div class="row">
          
        <?php if($this->getRequest()->getSession()->read('Auth')) : ?>
                                   <div style="float: right; margin-right: 9%">
                                <?= $this->Html->link(__('Salir'),['controller'=>'Admins',  'action'=>'logout'],['class'=>'btn btn-danger btn-sm'])?>
                                </div>
                                <div  style="float: right; ">
                                <?= $this->Html->link(__('Cambio de contraseña'), ['controller'=>'Admins','action' => 'cambio'],['class'=>'btn btn-link','style'=>'color:#f5f5f5']) ?>
                                </div>
                                <div >

                                    <?= $this->Html->link($adminUser->correo,['controller'=>'Admins','action'=>'view',$adminUser->id],['class'=>'btn btn-sm btn-link','style'=>'color:#f5f5f5'])?>
                                </div>
           <?php endif; ?>  

     </div>
</header>

