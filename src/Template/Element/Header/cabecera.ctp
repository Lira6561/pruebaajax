<header class="header" id="header" role="banner">
    <?php
        echo $this->Html->link(
            $this->Html->image(
                'https://www.unam.mx/sites/all/themes/unam/logo.png',
                ['alt' => 'Inicio']
            ),
            ['controller' => 'recursos', 'action' => 'index'],
            ['class' => 'header__logo-image', 'escape' => false]
        );
    ?>
</header>
